<?php

function proxy($url){
	$ch = curl_init();
	$options = [
		CURLOPT_URL => $url,
		CURLOPT_POST => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_BINARYTRANSFER => true,
		CURLOPT_HEADER => false,
		CURLOPT_NOBODY => false,
		CURLOPT_POSTFIELDS => $_POST
	];
	curl_setopt_array($ch, $options);
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

function proxySocket($url){
	$port = getservbyname('www', 'tcp');
	$url = gethostbyname($url);

	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if($socket === false){
		return false;
	}
	$connect = socket_connect($socket, $url, $port);
	if($connect === false){
		return false;
	}

	$data = '';
	foreach ($_POST as $key => $value) {
		$data .= $key."=".$value."&";
	}

	$data = urlencode($data);
	$rn = "\r\n";
	$in = "POST / HTTP/1.1".$rn;
	$in .= "Accept:text/html".$rn;
	$in .= "Host: ".$url.$rn;
	$in .= "Content-Type: application/x-www-form-urlencoded".$rn;
	$in .= "Content-Length: ".strlen($data).$rn;
	$in .= "Connection: close".$rn.$rn;
	$in .= $data.$rn.$rn;

	$write = socket_write($socket, $in, strlen($in));
	if($write === false){
		return false;
	}

	$result = '';
	$out = '';
	while ($out = socket_read($socket, 64)) {
	    $result .= $out;
	}
	if($out === false){
		return false;
	}
	
	socket_close($socket);
	$result = explode("\r\n\r\n", $result, 2)[1];
	return $result;
}